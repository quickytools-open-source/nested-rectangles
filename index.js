#!/usr/bin/env node

const fs = require('fs')
const args = require('args')

args.option('width', 'Image width', 1920)
args.option('height', 'Image height', 1080)
args.option('padding', 'Nested padding', 128)
args.option('color1', 'First color (outer)', '#42A5F5')
args.option('color2', 'Second color (alternating)', '#EC407A')
const flags = args.parse(process.argv)
const {
  width: imageWidth,
  height: imageHeight,
  padding: rectanglePadding,
  color1,
  color2,
} = flags

// TODO Create canvas of image width and height

let width = imageWidth
let height = imageHeight
let offset = 0
const colors = [color1, color2]
let index = 0
while (width > 0 && height > 0) {
  const color = colors[index % colors.length]
  console.log(width, height, offset, color)
  width -= rectanglePadding
  height -= rectanglePadding
  offset += rectanglePadding
  index++
  // TODO Create rectangles of alternating colors
  /*
   * Width and height
   * Nested padding
   * Alternating colors
   * Nested level over each side
   */
}
